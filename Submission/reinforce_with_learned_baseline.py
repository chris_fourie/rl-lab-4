# Adapted from: https://github.com/andrecianflone/rl_at_ammi/blob/master/REINFORCE-learned-baseline_solution.ipynb
# Env fix: https://stackoverflow.com/questions/44198228/install-pybox2d-for-python-3-6-with-conda-4-3-21
# conda install swig # needed to build Box2D in the pip install
# pip install box2d-py # a repackaged version of pybox2d

import gym
import numpy as np
from collections import deque
import random

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.distributions import Categorical
import matplotlib.pyplot as plt

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# configure matplotlib
plt.rcParams['figure.figsize'] = (15.0, 10.0) # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

class PolicyValueNetwork(nn.Module):
    def __init__(self):
        super(PolicyValueNetwork, self).__init__()
        self.fc_shared = nn.Linear(8, 128)
        self.fc_policy = nn.Linear(128, 4)
        self.fc_value_function = nn.Linear(128, 1)

    def forward(self, x):
        x = F.relu(self.fc_shared(x))
        value = self.fc_value_function(x)
        logits = self.fc_policy(x)
        dist = Categorical(logits=logits)
        return dist, value


def compute_returns(rewards, gamma):
    R = 0
    returns = []
    for step in reversed(range(len(rewards))):
        R = rewards[step] + gamma * R
        returns.insert(0, R)
    returns = np.array(returns)
    returns -= returns.mean()
    returns /= returns.std()
    return returns


def reinforce_learned_baseline(env, policy_model, seed, learning_rate,
                               number_episodes,
                               gamma, verbose=False):
    # set random seeds (for reproducibility)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    env.seed(seed)
    random.seed(seed)

    # instantiate the policy and optimizer
    net = policy_model.to(device)
    optimizer = optim.Adam(net.parameters(), lr=learning_rate)

    scores = []
    scores_deque = deque(maxlen=50)
    for episode in range(1, number_episodes + 1):

        log_probs = []
        values = []
        rewards = []

        state = env.reset()
        for t in range(1000):
            state = torch.from_numpy(state).float().to(device)
            dist, value = net(state)
            action = dist.sample()
            log_prob = dist.log_prob(action)
            state, reward, done, _ = env.step(action.item())
            rewards.append(reward)
            log_probs.append(log_prob.unsqueeze(0))
            values.append(value)

            if done:
                break

        scores.append(sum(rewards))
        scores_deque.append(sum(rewards))
        returns = compute_returns(rewards, gamma)
        returns = torch.from_numpy(returns).float().to(device)
        values = torch.cat(values)
        log_probs = torch.cat(log_probs)
        delta = returns - values

        policy_loss = -torch.sum(log_probs * delta.detach())
        value_function_loss = 0.5 * torch.sum(delta ** 2)
        loss = policy_loss + value_function_loss
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if episode % 50 == 0:
            print('Episode {}\tAverage Score: {:.2f}'.format(episode, np.mean(scores_deque)))

    return net, scores


def main():
    env = gym.make('LunarLander-v2')
    print('Action space: ', env.action_space)
    print('Observation space: ', env.observation_space)

    # hyper-parameters
    gamma = 0.99
    learning_rate = 0.02
    # seed = 214
    seed = 401
    number_episodes = 1250
    policy_model = PolicyValueNetwork()

    net, scores = reinforce_learned_baseline(env, policy_model, seed, learning_rate,
                                             number_episodes,
                                             gamma, verbose=True)

    env = gym.wrappers.Monitor(env, './video/', video_callable=lambda episode_id: episode_id, force=True)
    state = env.reset()
    for t in range(2000):
        state = torch.from_numpy(state).float().to(device)
        dist, value = net(state)
        action = dist.sample().item()
        env.render()
        state, reward, done, _ = env.step(action)
        if done:
            state = env.reset()
    env.close()


if __name__ == '__main__':
    main()
