#adapted from: https://github.com/seungeunrho/minimalRL/blob/master/REINFORCE.py
#adapted from: https://github.com/andrecianflone/rl_at_ammi/blob/master/REINFORCE_solution.ipynb

import numpy as np
import matplotlib.pyplot as plt
import gym

import torch
import torch.nn as nn

import random
from collections import deque

###
import torch.optim as optim
import torch.nn.functional as F
from torch.distributions import Categorical


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
plt.rcParams['figure.figsize'] = (15.0, 10.0) # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

# Hyperparameter
num_episodes = 1500

class SimplePolicy(nn.Module):
    def __init__(self, s_size=4, h_size=16, a_size=2):
        super(SimplePolicy, self).__init__()
        self.fc1 = nn.Linear(s_size, h_size)
        self.fc2 = nn.Linear(h_size, a_size)


    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = self.fc2(x) ###
        return Categorical(logits=x)


def moving_average(a, n):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret / n


def compute_returns(rewards, gamma):
    returns = 0
    for step in range(len(rewards)):
        returns += gamma ** step * rewards[step]
    return returns


def reinforce(env, policy_model, seed, learning_rate,
              number_episodes,
              max_episode_length,
              gamma, verbose=True):
    # set random seeds (for reproducibility)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)
    env.seed(seed)
    policy = policy_model.to(device)
    optimizer = optim.Adam(policy.parameters(), lr=learning_rate)

    scores = []
    scores_deque = deque(maxlen=100)
    for episode in range(1, number_episodes + 1):
        log_probs = []
        rewards = []
        state = env.reset()
        for t in range(max_episode_length):
            dist = policy(torch.from_numpy(state).float().to(device))
            action = dist.sample()
            log_prob = dist.log_prob(action).unsqueeze(0)
            state, reward, done, _ = env.step(action.item())
            rewards.append(reward)
            log_probs.append(log_prob)
            if done:
                break

        scores.append(sum(rewards))
        scores_deque.append(sum(rewards))
        returns = compute_returns(rewards, gamma)
        log_probs = torch.cat(log_probs)
        policy_loss = -torch.sum(log_probs * returns)
        optimizer.zero_grad()
        policy_loss.backward()
        optimizer.step()

        if episode % 50 == 0 and verbose:
            print('Episode {}\tAverage Score: {:.2f}'.format(episode, np.mean(scores_deque)))

    return policy, scores

def moving_average(a, n) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret / n

def compute_returns_naive_baseline(rewards, gamma):
    r = 0
    returns = []
    for step in reversed(range(len(rewards))):
        r = rewards[step] + gamma * r
        returns.insert(0, r)
    returns = np.array(returns)
    returns -= returns.mean()
    returns /= returns.std()
    return returns

def reinforce_naive_baseline(env, policy_model, seed, learning_rate,
                             number_episodes,
                             max_episode_length,
                             gamma, verbose=True):

    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    env.seed(int(seed))
    policy = policy_model.to(device)
    optimizer = optim.Adam(policy.parameters(), lr=learning_rate)

    scores = []
    scores_deque = deque(maxlen=100)
    for episode in range(1, number_episodes + 1):
        log_probs = []
        rewards = []
        state = env.reset()
        for t in range(max_episode_length):
            dist = policy(torch.from_numpy(state).float().to(device))
            action = dist.sample()
            log_prob = dist.log_prob(action).unsqueeze(0)
            state, reward, done, _ = env.step(action.item())
            rewards.append(reward)
            log_probs.append(log_prob)
            if done:
                break

        scores.append(sum(rewards))
        scores_deque.append(sum(rewards))
        returns = compute_returns_naive_baseline(rewards, gamma)
        returns = torch.from_numpy(returns).float().to(device)
        log_probs = torch.cat(log_probs)
        policy_loss = -torch.sum(log_probs * returns)
        optimizer.zero_grad()
        policy_loss.backward()
        optimizer.step()

        if episode % 50 == 0 and verbose:
            print('Episode {}\tAverage Score: {:.2f}'.format(episode, np.mean(scores_deque)))

    return policy, scores



def run_reinforce():
    env = gym.make('CartPole-v1')
    policy_model = SimplePolicy(s_size=env.observation_space.shape[0], h_size=50, a_size=env.action_space.n)

    policy, scores = reinforce(env=env, policy_model=policy_model, seed=42, learning_rate=1e-2,
                               number_episodes=num_episodes,
                               max_episode_length=1000,
                               gamma=1.0,
                               verbose=True)
    env.close()

    # Plot learning curve
    fig = plt.figure()
    ax = fig.add_subplot(111)
    x = np.arange(1, len(scores) + 1)
    ax.plot(x, scores, label='Score')
    m_average = moving_average(scores, 50)
    ax.plot(x, m_average, label='Moving Average (w=100)', linestyle='--')
    plt.legend()
    plt.ylabel('Score')
    plt.xlabel('Episode #')
    plt.title('REINFORCE learning curve - CartPole-v1')
    plt.show()

    #visualize
    # env = gym.make('CartPole-v1')
    # state = env.reset()
    # for t in range(2000):
    #     dist = policy(torch.from_numpy(state).float().to(device))
    #     action = dist.sample()
    #     env.render()
    #     state, reward, done, _ = env.step(action.item())
    #     if done:
    #         break
    # env.close()


def investigate_variance_in_reinforce():
    env = gym.make('CartPole-v1')
    seeds = np.random.randint(1000, size=5)

    policy_model = SimplePolicy(s_size=env.observation_space.shape[0], h_size=50, a_size=env.action_space.n)

    all_scores = []
    for seed in seeds:
        print("started training with seed: ", seed)
        _, scores = reinforce(env=env, policy_model=policy_model, seed=int(seed), learning_rate=1e-2,
                               number_episodes=num_episodes,
                               max_episode_length=1000,
                               gamma=1.0,
                               verbose=True)
        print("completed training with seed: ", seed)
        all_scores.append(scores)
    env.close()

    smoothed_scores = [moving_average(s, 50) for s in all_scores]
    smoothed_scores = np.array(smoothed_scores)
    mean = smoothed_scores.mean(axis=0)
    std = smoothed_scores.std(axis=0)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    x = np.arange(1, len(mean) + 1)
    ax.plot(x, mean, '-', color='blue')
    ax.fill_between(x, mean - std, mean + std, color='blue', alpha=0.2)
    plt.ylabel('Score')
    plt.xlabel('Episode #')
    plt.title('REINFORCE averaged over 5 seeds')
    plt.show()

    return mean, std


def run_reinforce_with_naive_baseline(mean, std):
    env = gym.make('CartPole-v1')
    np.random.seed(53)
    seeds = np.random.randint(1000, size=5)

    policy_model = SimplePolicy(s_size=env.observation_space.shape[0], h_size=50, a_size=env.action_space.n)

    all_scores_baseline = []
    for seed in seeds:
        print("started training with seed: ", seed)
        _, scores = reinforce_naive_baseline(env=env, policy_model=policy_model, seed=seed, learning_rate=1e-2,
                               number_episodes=num_episodes,
                               max_episode_length=1000,
                               gamma=1.0,
                               verbose=True)
        print("completed training with seed: ", seed)
        all_scores_baseline.append(scores)
    env.close()

    # comparing the methods
    smoothed_scores_baseline = [moving_average(s, 50) for s in all_scores_baseline]
    smoothed_scores_baseline = np.array(smoothed_scores_baseline)
    mean_baseline = smoothed_scores_baseline.mean(axis=0)
    std_baseline = smoothed_scores_baseline.std(axis=0)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    x = np.arange(1, len(mean_baseline) + 1)
    ax.plot(x, mean_baseline, '-', color='green', label='Variance reduced REINFORCE')
    ax.plot(x, mean, '-', color='blue', label='REINFORCE')
    ax.fill_between(x, mean_baseline - std_baseline, mean_baseline + std_baseline, color='green', alpha=0.2)
    ax.fill_between(x, mean - std, mean + std, color='blue', alpha=0.2)
    plt.ylabel('Score')
    plt.xlabel('Episode #')
    plt.legend()
    plt.title('Comparison of REINFORCE and Variance reduced REINFORCE (averaged over 5 seeds)')
    plt.show()


if __name__ == '__main__':
    run_reinforce()
    mean, std = investigate_variance_in_reinforce()
    print ("MEAN: ", mean)
    run_reinforce_with_naive_baseline(mean, std)
