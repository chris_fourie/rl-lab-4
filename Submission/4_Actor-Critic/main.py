# adpated from: https://github.com/sfujim/TD3

import numpy as np
import torch
import gym
import argparse
import os

import RB  # replay buffer
import TD3


# Runs policy for X episodes and returns average reward
# A fixed seed is used for the eval environment

# Evaluate Policy and reuturn an average reward over number of evaluations
def eval_policy(policy, env_name, seed, eval_episodes=2):
    eval_env = gym.make(env_name)
    eval_env.seed(seed + 100)

    avg_reward = 0.
    for _ in range(eval_episodes):
        state, done = eval_env.reset(), False
        while not done:
            action = policy.select_action(np.array(state))
            state, reward, done, _ = eval_env.step(action)
            avg_reward += reward

    avg_reward /= eval_episodes

    print("---------------------------------------")
    print(f"Evaluation over {eval_episodes} episodes: {avg_reward:.3f}")
    print("---------------------------------------")
    return avg_reward


if __name__ == "__main__":

    ## PART 1 - Setup
    hp = dict(policy="TD3",
              env="BipedalWalker-v2",
              seed=0,  # Sets Gym, PyTorch and Numpy seeds
              start_timesteps=1e4,  # Time steps initial random policy is used
              eval_freq=100,  # How often (number of episodes) we evaluate
              max_timesteps=1e6,  # Max time steps to run environment
              expl_noise=0.1,  # Std of Gaussian exploration noise
              batch_size=256,  # Batch size for both actor and critic
              discount=0.99,  # Discount factor
              tau=0.005,  # Target network update rate
              policy_noise=0.2,  # Noise added to target policy during critic update
              noise_clip=0.2,  # Range to clip target policy noise
              policy_freq=2,  # Frequency of delayed policy updates
              save_model="store_true",  # Save model and optimizer parameters
              load_model="default")  # Model load file name, "" doesn't load, "default" uses file_name




    # setup saving model and results
    file_name = f"{hp['policy']}_{hp['env']}_{hp['seed']}"
    print("---------------------------------------")
    print(f"Policy: {hp['policy']}, Env: {hp['env']}, Seed: {hp['seed']}")
    print("---------------------------------------")

    if not os.path.exists("./results"):
        os.makedirs("./results")

    if hp['save_model'] and not os.path.exists("./models"):
        os.makedirs("./models")

    # set env
    env = gym.make(hp['env'])
    # use video wrapper - record every evaluation
    # env = gym.wrappers.Monitor(env, './video/', video_callable=lambda episode_id: episode_id, force=True)
    env = gym.wrappers.Monitor(env, './video/', video_callable=lambda episode_id: episode_id % hp['eval_freq'] == 0, force=True)

    # Set seeds
    env.seed(hp['seed'])
    torch.manual_seed(hp['seed'])
    np.random.seed(hp['seed'])

    # set dimensions of spaces
    state_dim = env.observation_space.shape[0]
    action_dim = env.action_space.shape[0]
    max_action = float(env.action_space.high[0])

    # setup policy
    policy_params = dict(state_dim=state_dim,
                         action_dim=action_dim,
                         max_action=max_action,
                         discount=hp['discount'],
                         tau=hp['tau'],
                         policy_noise=hp['policy_noise'] * max_action,
                         noise_clip=hp['noise_clip'] * max_action,
                         policy_freq=hp['policy_freq'])

    # instantiate policy
    policy = TD3.TD3(**policy_params)

    # load previously learnt policy if desired
    if hp['load_model'] != "":
        policy_file = file_name if hp['load_model'] == "default" else hp['load_model']  # choose policy file
        policy.load(f"./models/{policy_file}")  # load learned policy

    # instantiate replay buffer for augmenting learning
    replay_buffer = RB.ReplayBuffer(state_dim, action_dim)

    # Evaluate untrained policy
    evaluations = [eval_policy(policy, hp['env'], hp['seed'])]

    ## PART 2 - GYM RUN
    state, done = env.reset(), False
    episode_reward = 0
    episode_timesteps = 0
    episode_num = 0

    for t in range(int(hp['max_timesteps'])):
        episode_timesteps += 1

        # Select action randomly or according to **Actor-Critic policy**
        if t < hp['start_timesteps']:
            action = env.action_space.sample()  # random action
        else:
            action = (policy.select_action(np.array(state))
                      + np.random.normal(0, max_action * hp['expl_noise'], size=action_dim)).clip(-max_action,
                                                                                                  max_action)

        # Perform action
        next_state, reward, done, _ = env.step(action)
        # done_bool = float(done) if episode_timesteps < env._max_episode_steps else 0 ### old code
        done_bool = float(done) if episode_timesteps < 1600 else 0

        # Store data for time step in replay buffer
        replay_buffer.add(state, action, next_state, reward, done_bool)

        state = next_state
        episode_reward += reward

        # Train agent after collecting sufficient data
        if t >= hp['start_timesteps']:
            policy.train(replay_buffer, hp['batch_size'])

        # End of Episode
        if done:
            # +1 to account for 0 indexing. +0 on ep_timesteps since it will increment +1 even if done=True
            print(
                f"Total T: {t + 1} Episode Num: {episode_num + 1} Episode T: {episode_timesteps} Reward: {episode_reward:.3f}")
            # Reset environment
            state, done = env.reset(), False
            episode_reward = 0
            episode_timesteps = 0
            episode_num += 1  # next episode

        # PART 3 - Record
        # Evaluate episode, save results and save model
        if (episode_num + 1) % hp['eval_freq'] == 0:
            evaluations.append(eval_policy(policy, hp['env'], hp['seed']))
            np.save(f"./results/{file_name}", evaluations)
            if hp["save_model"]: policy.save(f"./models/{file_name}", episode_num)
            episode_num += 1
